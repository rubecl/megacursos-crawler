const Helpers = require('../class/Helpers.js');

class Video {
    constructor() {
        this.nombre_seccion = null
        this.numero = null
        this.nombre = null
        this.descripcion = null
        this.miniatura = null
        this.video = null
        this.archivos = null
        
        this.descargado = false
        this.peso = 0
    }
    
    /**
     * Lee un objeto HTML generado desde "cheerio"
     * @param {type} root Objeto generado desde cheerio.load(HTML)
     * @param {type} video Objeto HTML de cheerio con la informacion del video actual
     *  Obtenido con: root('.panel-body .boxRow', seccion)[n]
     */
    parseHTML(root, video) {

        this.miniatura = root('.imgLeft img', video).attr('src')
        
        let nombre = root('.contentRight strong', video).html()
        nombre = nombre.split("|")
        this.numero = nombre[0].trim()
        this.nombre = Helpers.decodeHTML(nombre[1].trim())
        
        Helpers.log("Obteniendo informacion del video " + this.numero + ": " + this.nombre)
        
        this.video = root('.contentRight a.changeVideo', video).attr('data-video_url')
        let archivos = root('.contentRight a[download]', video).attr('href')
        
        
        /*if (archivos == 'http://mc-products.s3.amazonaws.com/courses/after-effects/megacurso/v2/p4/ArchivosServidorExterno.txt') {
            Helpers.log("Los archivos del video son compartidos del curso")
        } else*/ if (archivos == "" || archivos == 'http://mc-products.s3.amazonaws.com/courses/no-class-files/archivos_clase.rar') {
            Helpers.log("El video no tiene archivos")
        } else {
            this.archivos = archivos;
        }
        
        // Intentamos cargar la descripcion, puede estar en varias capas diferentes...
        let descripcion = root('.contentRight p:nth-child(3)', video).html()
        if (descripcion == "" || descripcion == null) {
            descripcion = root('.contentRight span.s1', video).html()
            if (descripcion == "" || descripcion == null) {
                descripcion = root('.contentRight .descr', video).html()
                if (descripcion == "" || descripcion == null) {
                    descripcion = root('.contentRight .page .column p', video).html()
                }
            }
        }
        
        // La descripcion puede haber cargado mal
        try {
            this.descripcion = Helpers.decodeHTML(descripcion.trim())
        } catch (ex) {
            throw ex;
        }
    }
}

module.exports = Video