// El request lo cargaremos con cookies que se guardaran 
// y con soporte para asíncronía (por eso la reescritura en este archivo)
const account = require('../config/account.js')
const FileCookieStore = require('tough-cookie-filestore'); // Almacen de cookies
var request = require('request')

if (account.SAVE_COOKIES) {
    let cookieFile = process.cwd() + '/cookies.data'
    console.log("Archivo de cookies: ", cookieFile)
    let fs = require('fs')

    // Creamos el archivo de cookies si no existe
    try {
        if (!fs.existsSync(cookieFile)) {
            fs.writeFileSync(cookieFile, "")
            console.log("Creado archivo de cookies:", cookieFile)
        }
    } catch (ex) {
        console.log("No se ha podido crear el archivo de cookies...")
        console.log(ex)
    }

    // Cargamos el archivo de cookies
    try {
        var j = request.jar(new FileCookieStore(cookieFile));
        request = request.defaults({jar: j }) // Cookies estaticas
    } catch (ex) {
        console.log("No se ha podido cargar archivo de cookies, se utilizaran cookies temporales")
        console.log(cookieFile)
        console.log(ex)
        request = request.defaults({jar: true}) // Cookies temporales
    }
} else {
    request = request.defaults({jar: true}) // Cookies temporales
}

// Generamos la sobreescritura de request
module.exports = async (value) => 
new Promise((resolve, reject) => {
    request(value, (error, response, data) => {
        if(error) reject(error)
        else resolve(data)
    })
})